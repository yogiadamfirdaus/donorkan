-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 14, 2020 at 05:30 AM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `donorkan`
--

-- --------------------------------------------------------

--
-- Table structure for table `darah`
--

CREATE TABLE `darah` (
  `id` int(11) NOT NULL,
  `pasien_id` int(11) NOT NULL,
  `golongan_darah` varchar(255) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `usia` int(11) NOT NULL,
  `jenis_kelamin` enum('laki-laki','perempuan') NOT NULL,
  `no_hp` varchar(255) DEFAULT NULL,
  `alamat` varchar(255) NOT NULL,
  `riwayat_penyakit` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `darah`
--

INSERT INTO `darah` (`id`, `pasien_id`, `golongan_darah`, `nama`, `usia`, `jenis_kelamin`, `no_hp`, `alamat`, `riwayat_penyakit`, `created_at`, `updated_at`) VALUES
(1, 2, 'A', 'bambang', 43, 'laki-laki', '08978990877', 'jalan danang selaran', 'covid', '2020-04-01 08:41:19', '2020-04-01 08:41:19'),
(2, 2, 'O', 'andi', 43, 'laki-laki', '08978990877', 'jalan danang selaran', 'herfes', '2020-04-01 08:43:44', '2020-04-01 08:43:44'),
(3, 2, 'A', 'banang', 43, 'perempuan', '08978990877', 'cepu', 'noting', '2020-04-01 08:49:14', '2020-04-01 08:49:14'),
(4, 5, 'B+', 'mamat', 38, 'laki-laki', '08973726112', 'jalan gunung tinggi sekali , no 12', 'tidak ada', '2020-04-04 20:35:24', '2020-04-04 20:35:24'),
(5, 5, 'AB', 'bandui', 57, 'perempuan', '08973726112', 'jalan tebing juram sekali, no 43', 'anemia', '2020-04-04 20:44:51', '2020-04-04 20:44:51');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `transaksi`
--

CREATE TABLE `transaksi` (
  `id` int(11) NOT NULL,
  `pendonor_id` int(11) NOT NULL,
  `pasien_id` int(11) NOT NULL,
  `darah_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transaksi`
--

INSERT INTO `transaksi` (`id`, `pendonor_id`, `pasien_id`, `darah_id`, `created_at`, `updated_at`) VALUES
(7, 5, 2, 1, '2020-04-07 19:19:05', '2020-04-07 19:19:05'),
(8, 5, 2, 1, '2020-04-13 01:56:00', '2020-04-13 01:56:00'),
(9, 2, 5, 4, '2020-04-13 02:01:32', '2020-04-13 02:01:32'),
(10, 4, 2, 3, '2020-04-13 02:22:02', '2020-04-13 02:22:02'),
(11, 5, 2, 1, '2020-04-13 08:20:19', '2020-04-13 08:20:19');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `akses` enum('customer','admin') COLLATE utf8mb4_unicode_ci DEFAULT 'customer',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `akses`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'nana', 'nana@gmail.com', NULL, '$2y$10$TZh7sUPk2YyZ6wRP.CVA2OfYlJKWKiGFbolQmCf7S9gOmf1zR6Age', 'admin', 'dDjiK8nDnLFO06XTukIe1PmV2FW6dt6WIvgxr57Y5lU0JjovEsGKZ899PHVO', '2020-03-27 20:04:43', '2020-03-31 18:41:57'),
(2, 'lala', 'lala@gmail.com', NULL, '$2y$10$xX7Djb4eWwvhLcG5IIG6vuijHyNmFKD2us5le7C2px7M56f9bW7C2', 'customer', 'oi4D9GoEt2fcwdCEwFGmTyckLxLWsX4R1z4CnBYgVtC27aofeKpaJcPTbXQE', '2020-03-31 18:58:58', '2020-03-31 18:58:58'),
(4, 'papa', 'papa@gmail.com', NULL, '$2y$10$p6VESdWh/tt4csm6ip6/E.cRJpSIuA8PCRp523N4.NSLMO282rGGu', 'customer', 'Oe1ok9GO8HzjPz2S5k6UggchLQ8zAUSXOzlfVgdUkmc7x47raSEOfkr6WYE5', '2020-03-31 19:06:27', '2020-03-31 19:06:27'),
(5, 'jaja', 'jaja@gmail.com', NULL, '$2y$10$IEJO8XcE7MjLuwjdAYlt..btJH7XKQn56LHCbkH7MmxkR7DqjUik6', 'customer', 'DzYjCOrZI0mFbE16xTbGH2DQDWInKLCRH9FxY7kq2u7uqFGO1PBXqZ4BSXqX', '2020-03-31 19:22:38', '2020-03-31 19:22:38');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `darah`
--
ALTER TABLE `darah`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transaksi`
--
ALTER TABLE `transaksi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `darah`
--
ALTER TABLE `darah`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `transaksi`
--
ALTER TABLE `transaksi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
