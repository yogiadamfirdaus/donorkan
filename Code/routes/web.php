<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
| 
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/Darah','DarahController@darah')->name('darah'); //setelah welcome / diluar home

Auth::routes();
Auth::routes(['verify' => true]);
//Route::get('/home', 'HomeController@index')->name('home');

Route::group(['prefix'=>'Home','middleware'=>['verified']], function() {

    Route::get('/Setting','UserSettingController@form')->name('name.donorkan.user.setting');
    Route::post('/Setting','UserSettingController@update');

            Route::group(['prefix'=>'Admin','middleware'=>'akses.admin'], function() { //admin
                Route::get('/Donor/Darah/{id}', 'AdminController@idpesan');

                Route::get('/', 'AdminController@dashboard')->name('home.admin');
                Route::get('/Pendonor','AdminController@pendonor')->name('name.donorkan.admin.pendonor');
                Route::get('/Pendonor/Detail/{id}', 'AdminController@iddetailpendonor');

                Route::get('/Jumlah/Pendonor','AdminController@jumlahpendonor')->name('name.donorkan.admin.jumlah.jumlahpendonor');
                Route::get('/Jumlah/Donor/Darah','AdminController@jumlahdonordarah')->name('name.donorkan.admin.jumlah. jumlahdonordarah');
                Route::get('/Jumlah/Minta/Darah','AdminController@jumlahmintadarah')->name('name.donorkan.admin.jumlah.jumlahmintadarah');
                Route::get('/Jumlah/Belum/Donor','AdminController@jumlahbelumdonor')->name('name.donorkan.admin.jumlah.jumlahbelumdonor');
                
                Route::get('/Managemen/User','AdminController@daftaruser')->name('name.donorkan.admin.user');
                Route::get('/Managemen/User/Hapus/{id}','AdminController@hapus');

                Route::get('/Managemen/Darah','AdminController@daftardarah')->name('name.donorkan.admin.darah'); 
                Route::get('/Managemen/Darah/Detail/{id}', 'AdminController@iddetail');
                Route::get('/Managemen/Darah/Hapus/{id}','AdminController@delete'); 

            });

            Route::group(['prefix'=>'','middleware'=>'akses.customer'], function() { //customer
                Route::get('/', 'TransaksiController@dashboard')->name('home');
                Route::get('/Minta/Darah','DarahController@form')->name('name.donorkan.darah.tambah');
                Route::post('/Minta/Darah','DarahController@tambah');

                Route::get('/Riwayat/Minta/Darah','DarahController@daftar')->name('name.donorkan.darah.daftar'); 
                Route::get('/Riwayat/Minta/Darah/{id}','DarahController@status')->name('name.donorkan.darahstatus');
                Route::post('/Riwayat/Minta/Darah/{id}','DarahController@statusupdate');
                Route::get('/Riwayat/Minta/Darah/Detail/{id}', 'DarahController@iddetail');

                    Route::group(['prefix'=>''],function() {
                        Route::get('/Donor/Darah/{id}', 'DarahController@idpesan');//ketika id nya diganti dengan id yang tidak ada dalam tabel langsung di url, maka masih eror
                        Route::get('/Donor/Darah','TransaksiController@form')->name('name.donorkan.transaksi.tambah');
                        Route::post('/Donor/Darah','TransaksiController@tambah');

                        Route::get('/Riwayat/Donor/Darah','TransaksiController@daftar')->name('name.donorkan.transaksi.daftar');
                        Route::get('/Riwayat/Donor/Darah/Detail/{id}', 'TransaksiController@iddetail');

                        Route::get('/Pendonor','TransaksiController@pendonor')->name('name.donorkan.transaksi.pendonor');
                        Route::get('/Pendonor/{id}','TransaksiController@konfirmasi')->name('name.donorkan.transaksi.pendonorstatus');
                        Route::post('/Pendonor/{id}','TransaksiController@konfirmasiupdate');
                
                    });
            });

});

