<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
//use Illuminate\Database\Eloquent\SoftDeletes;

class Darah extends Model
{
  //use SoftDeletes;
    protected $table = 'darah';
    protected $guarded = [];
   // protected $hidden = "users_id";
   //protected $dates = ['deleted_at'];

    public function transaksi(){
        return $this->hasMany('App\Transaksi');
        }
        public function lokasi(){
            return $this->hasOne('App\Lokasi');
            }
         //   public function lokasi(){
           //     return $this->belongsTo('App\Lokasi', 'lokasi_id');
             //   }
                 public function user(){
                return $this->belongsTo('App\User', 'users_id');
                }
         //       public function t(){
           //       return $this->belongsTo('App\Transaksi', 'users_id');
             //     }
}