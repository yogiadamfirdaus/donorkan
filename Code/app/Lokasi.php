<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lokasi extends Model
{
    protected $table = 'lokasi';
    protected $guarded = [];

  //  protected $fillable = [
    //    'alamat', 'latitude', 'longitude',
    //];
  //  public $appends = [
    //    'coordinate', 'map_popup_content',
    //];
    
    public function darah(){
        return $this->belongsTo('App\Darah', 'darah_id');
        }

      //  public function darah(){
        //    return $this->hasOne('App\Darah');
          //  }
      //    public function transaksi(){
        //       return $this->hasMany('App\Transaksi');
          //    }

}
