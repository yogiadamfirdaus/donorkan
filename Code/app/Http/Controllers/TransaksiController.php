<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Transaksi;
use App\Darah;
use App\User;
use Illuminate\Support\Facades\Crypt;
use DB;
use Session;
use Illuminate\Contracts\Encryption\DecryptException;

class TransaksiController extends Controller
{
        public function dashboard(){  //dashboard
            
            //menampilkan sesuai id user yg login
          // $transaksi = Transaksi::where(['pasien_id' => auth()->user()->id])->distinct()->get(['darah_id']);

         
          // $r = Darah::distinct()->get(['nama']);
          //gak sido
          // $transaksi = Darah::where(['pasien_id' => auth()->user()->id])->distinct()->get(['nama']);

          //manampilkan nama pasien yg sudah ada pendonor yg masuk
           $transaksi5 = Transaksi::where(['users_id' => auth()->user()->id])->distinct()->get(['darah_id']);
           
           $transaksi51 = Transaksi::select( 'users.name', 'darah.nama')
           ->join('darah', 'darah.id', '=', 'transaksi.darah_id')
           ->join('users', 'users.id', '=', 'transaksi.pendonor_id')
           ->where('transaksi.users_id', '=', auth()->user()->id)->get();
           $transaksi52 = Transaksi::where(['users_id' => auth()->user()->id])->distinct()->get(['darah_id']);
        //   where(['pasien_id' => auth()->user()->id])->get();
           //gagal, mau menampilkan jumlah pendonor sesuai nama pasien
         //   $task63 = Darah::select('id');
          //  $task6 = Transaksi::select('darah_id');
         //   $task62 = Transaksi::where('darah_id', '=', $task63 );
          //  $transaksi6 = $task62->where(['pasien_id' => auth()->user()->id])->get();
         //   $transaksi6 = $task62->where(['pasien_id' => auth()->user()->id])->get();
         //   $transaksi6 = Transaksi::where(['pasien_id' => auth()->user()->id])->get();

       //  $transaksi6 = DB::table('users')
       //  ->where('users.id', '=', 'transaksi.pendonor_id')->get();
      
       //  $transaksi6 = DB::table('transaksi')
         //->join('darah', 'darah.id', '=', 'transaksi.darah_id')
         //->join('users', 'users.id', '=', 'transaksi.pendonor_id')
         //->select('darah.*', 'darah.nama', 'users.name')->get();
     
        // ->join('darah', 'darah.id', '=', 'transaksi.darah_id')
         //->join('users', 'users.id', '=', 'transaksi.pendonor_id')
         //->select('darah.*', 'darah.nama', 'users.name')->get();

         //  $transaksi6 = $task67->where('pasien_id', '=', auth()->user()->id);
       
       $transaksi6 = DB::table('transaksi')->where(['users_id' => auth()->user()->id])->distinct()->count('darah_id');
        //menghitung data yang sama
       // $transaksi6 =Transaksi::where(['pasien_id' => auth()->user()->id])->distinct()->get(['darah_id']);
       

     //   $transaksi6 = Transaksi::where(['pasien_id' => auth()->user()->id])
       // ->join('users', 'users.id', '=', 'transaksi.pendonor_id')
        //->join('darah', 'darah.id', '=', 'transaksi.darah_id')
        //->select('users.*', 'users.name', 'darah.nama')->get();

     //    $transaksi6 = DB::table('transaksi')->whereExists(function ($query) {
       //    $query->select(DB::raw('name'))->from('users')->whereRaw('transaksi.pendonor_id = users.id');
         //})->get();

          // $task1 = Transaksi::select('pasien_id')->distinct()->get();
          // $transaksi = $task1->where(['pasien_id' => auth()->user()->id]);



            //menampilkan semua jumlah pendonor
            $task2 = Transaksi::select('pendonor_id');
            $transaksi2 = $task2->where(['users_id' => auth()->user()->id]);

            //menampilkan semua jumlah donor darah          yg sudah duilakukan
          //  $task3 = Transaksi::select('konfirmasi', '=', 'SudahDonor'); // ganti query "select konfirmasi dimana konfirmasi = SudahDonor"
            $task3 = Transaksi::where('konfirmasi', '=', 'SudahDonor');
            $transaksi3 = $task3->where(['pendonor_id' => auth()->user()->id]);

             //menampilkan semua jumlah donor darah          yg belum duilakukan
          $task32 = Transaksi::where('konfirmasi', '=', 'BelumDonor');
          $transaksi32 = $task32->where(['pendonor_id' => auth()->user()->id]);


             //menampilkan semua jumlah donor darah          yg sudah duilakukan
           //  $task3 = Transaksi::select('pendonor_id'); // ganti query "select konfirmasi dimana konfirmasi = SudahDonor"
             //$transaksi3 = $task3->where(['pendonor_id' => auth()->user()->id]);

             //menampilkan semua jumlah minta darah
             $task4 = Darah::select('id');
             $transaksi4 = $task4->where(['users_id' => auth()->user()->id]);
             


            return view('home', compact( 'transaksi2', 'transaksi3', 'transaksi4', 'transaksi5', 'transaksi6', 'transaksi51', 'transaksi52', 'transaksi32'));
            
        }
        
        public function daftar(){ //riwayat donor darah
           $transaksi = Transaksi::where(['pendonor_id' => auth()->user()->id])->get();
         //  $transaksi = Transaksi::with('user')->get();
            return view('donorkan.transaksi.daftar', compact('transaksi'));
         //  return view('donorkan.transaksi.daftar',['transaksi'=>$transaksi]);
         //->('pendonor_id')::count()
         //->where('pendonor_id')::count()->get()
        }

        public function iddetail($id)//riwayat donor darah - detail
        {
            try{
            $id = Crypt::decrypt($id);
            $dt = Darah::findOrFail($id);
            return view('donorkan.transaksi.detail',compact('dt'));
            }catch (DecryptException $e) {
                    return abort(404);
            }
         }
       

        public function form(){
            return view('donorkan.transaksi.tambah');
        }

        public function tambah(Request $request){
            $this->validate($request, [
           // 'pendonor_id'=>'',
           // 'users_id'=>'',
            //'darah_id'=>'',
         //   'kontak_pendonor'=>'required|numeric|max:9999999999999', //lebih baik diganti saja (dengan golongan darah)
            ]);
            Transaksi::create([
           // 'pendonor_id' => $request->pendonor_id,
           // 'users_id' => $request->users_id,
           // 'darah_id' => $request->darah_id,
            'pendonor_id' => Auth()->id(),
            'users_id' => Session::get('users_id'),
            'darah_id' => Session::get('id'),
          //  'kontak_pendonor' => $request->kontak_pendonor, //lebih baik diganti saja
            ]);
            return redirect()->route('name.donorkan.transaksi.daftar');
        }



        //daftar pendonor
        public function pendonor(Request $req){ 
          $pendonor = Transaksi::where(['users_id' => auth()->user()->id])->get();
           return view('donorkan.transaksi.pendonor', compact('pendonor'));
       }
       public function konfirmasi($id) //daftar yg menampilkan ubah konfirmasi
       {
         $data=Transaksi::where('id', $id)->first();
           return view('donorkan.transaksi.pendonor', compact('pendonor'));
           //return view('jastipkan.tempat');
       }
       public function konfirmasiupdate(Request $req)
       {
           \Validator::make($req->all(), 
           [
               'konfirmasi'=>'required',
           ])->validate();
               $field = [
                   'konfirmasi'=>$req->konfirmasi,
               ];
           $result = Transaksi::where('id',$req->id)->update($field);
           if($result) {
               return redirect()->route('name.donorkan.transaksi.pendonor')->with('result', 'update');
           } else {
               return back()->with('result', 'fail');
           }
       }


}