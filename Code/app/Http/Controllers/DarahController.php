<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Darah;
use App\Lokasi;
use App\Transaksi;
use Illuminate\Support\Facades\Crypt;
use Session;
use Illuminate\Contracts\Encryption\DecryptException;
class DarahController extends Controller
{


        public function idpesan($id)//menampilkan id tempat yg di klik
        {
            try{
            $id = Crypt::decrypt($id);
          //  $dt=Darah::where('id', $id)->first();
          $dt = Darah::findOrFail($id);

             //   $users_id = Darah::select('users_id')->where('id', $id)->first();
             $users_id = $dt->users_id;
                Session::put('users_id', $users_id);

             //   $ido = Darah::select('id')->where('id', $id)->first();
             $ido = $dt->id;
                Session::put('id', $ido);
            return view('donorkan.transaksi.tambah',compact('dt'));
            
            }catch (DecryptException $e) {
                    return abort(404);
            }
         }

        public function darah(){ //menampilkan darah di awal
            $data = Darah::get();
            return view('darah', compact('data'));
        }
        public function daftar(){  //riwayat minta darah
            $transaksi = Darah::where(['users_id' => auth()->user()->id])->get();
            return view('donorkan.darah.daftar', compact('transaksi'));
        }
        public function status($id) //daftar yg menampilkan ubah status
        {
          $data=Transaksi::where('id', $id)->first();
            return view('donorkan.darah.daftar', compact('pendonor'));
            //return view('jastipkan.tempat');
        }
        public function statusupdate(Request $req)
        {
            \Validator::make($req->all(), 
            [
                'status'=>'required',
            ])->validate();
                $field = [
                    'status'=>$req->status,
                ];
            $result = Darah::where('id',$req->id)->update($field);
            if($result) {
                return redirect()->route('name.donorkan.darah.daftar')->with('result', 'update');
            } else {
                return back()->with('result', 'fail');
            }
        }
 

        public function form(){
           return view('donorkan.darah.tambah');
        }

        public function tambah(Request $request){
            $this->validate($request, [
          //  'users_id'=> Auth::user()->id() ,
          'golongan_darah'=>'required|regex:/^[a-zA-Z+-]{1,5}$/',
          'nama'=>'required|regex:/^[a-zA-Z ]{3,50}$/',
          'usia'=>'required|regex:/^[0-9]{1,3}$/',
            'jenis_kelamin'=>'required',
           // 'no_hp'=>'required',//spertinya dibuat opsional saja
           // 'alamat'=>'required|between:1,100', //memakai share lokasi
          'riwayat_penyakit'=>'required|regex:/^[a-zA-Z, ]{3,50}$/',

          'alamat'=>'required|regex:/^[a-zA-Z0-9, ]{3,200}$/',
            'latitude'  => 'required|max:255',
            'longitude' => 'required|max:255',
            ]);
           $d= Darah::create([
           // 'users_id' => $request->users_id,
            'users_id' => Auth()->id(),
            'golongan_darah' => $request->golongan_darah,
            'nama' => $request->nama,
            'usia' => $request->usia,
            'jenis_kelamin' => $request->jenis_kelamin,
           // 'no_hp' => $request->no_hp, //spertinya dibuat opsional saja
           // 'alamat' => $request->alamat, // memakai share lokasi
            'riwayat_penyakit' => $request->riwayat_penyakit,
            ]);
            $l= Lokasi::create([
                'alamat' => $request->alamat,
                'latitude' => $request->latitude,
                'longitude' => $request->longitude,
            ]);
            $d->lokasi()->save($l);
          //  $l->darah()->save($d);
            return redirect()->route('name.donorkan.darah.daftar');
        }

        public function iddetail($id)//menampilkan id tempat yg di klik
        {
            try{
            $id = Crypt::decrypt($id);
            $dt = Darah::findOrFail($id);
            return view('donorkan.darah.detail',compact('dt'));
            }catch (DecryptException $e) {
                    return abort(404);
            }
         }
}