<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Darah;
use App\Lokasi;
use App\Transaksi;
use Illuminate\Support\Facades\Crypt;
use Session;
use Auth;
use Illuminate\Support\Str;
use Illuminate\Contracts\Encryption\DecryptException;
class AdminController extends Controller
{
    
         public function idpesan($id)//menampilkan id tempat yg di klik
         {
            try{
            $id = Crypt::decrypt($id);
            $dt = Darah::findOrFail($id);
            return view('donorkan.admin.lihat',compact('dt'));
            }catch (DecryptException $e) {
               return abort(404);
            }
         }

         //Dashbboard
         public function dashboard()
         {
            $task2 = Transaksi::select('pendonor_id');  //menampilkan semua jumlah pendonor
            $task3 = Transaksi::where('konfirmasi', '=', 'SudahDonor');    //menampilkan semua jumlah donor darah yg sudah duilakukan
            $task4 = Darah::select('id');  //menampilkan semua jumlah minta darah
            $task5 = Transaksi::distinct()->get(['darah_id']);
            $task6 = Transaksi::where('konfirmasi', '=', 'BelumDonor'); //menampilkan semua jumlah donor darah yg belum duilakukan
          return view('donorkan.admin.dashboard', compact( 'task2', 'task3', 'task4', 'task5', 'task6'));
         }

         public function pendonor(){ 
            $pendonor = Transaksi::get();
             return view('donorkan.admin.pendonor', compact('pendonor'));
         }
         public function iddetailpendonor($id)
         {
            try{
            $id = Crypt::decrypt($id);
            $dt = Transaksi::findOrFail($id);
            return view('donorkan.admin.detailpendonor',compact('dt'));
            }catch (DecryptException $e) {
               return abort(404);
            }
         }

         public function jumlahpendonor(){
            $task1 = Transaksi::get();
            return view('donorkan.admin.jumlah.jumlahpendonor', compact('task1'));
         }
         public function jumlahdonordarah(){
            $task2 = Transaksi::where('konfirmasi', '=', 'SudahDonor')->get(); 
            return view('donorkan.admin.jumlah.jumlahsudahdonor', compact('task2'));
         }
         public function jumlahmintadarah(){
            $task3 = Darah::get();
            return view('donorkan.admin.jumlah.jumlahmintadarah', compact('task3'));
         }
         public function jumlahbelumdonor(){
            $task4 = Transaksi::where('konfirmasi', '=', 'BelumDonor')->get(); 
            return view('donorkan.admin.jumlah.jumlahbelumdonor', compact('task4'));
         }

         //managemen user
         public function daftaruser(Request $req)
         {
             $data = User::get();
             return view('donorkan.admin.user',['data'=>$data]);
         }
         public function hapus($id)
         {
            try{
               $idi = Crypt::decrypt($id);
               User::find($idi)->delete();
               return redirect('/Home/Admin/Managemen/User'); //ganti
            }catch (DecryptException $e) {
               return abort(404);
            }
         }

         //managemen Darah
         public function daftardarah()
         {  
            $transaksi = Darah::get();
            return view('donorkan.admin.darah', compact('transaksi'));
         }
         public function iddetail($id)
         {
            try{
            $id = Crypt::decrypt($id);
            $dt = Darah::findOrFail($id);
            return view('donorkan.admin.detaildarah',compact('dt'));
            }catch (DecryptException $e) {
               return abort(404);
            }
         }
         public function delete($id)
         {
            try{
               $idi = Crypt::decrypt($id);
               Darah::find($idi)->delete();
               return redirect('/Home/Admin/Managemen/Darah'); //ganti
            }catch (DecryptException $e) {
               return abort(404);
            }
         }



}
