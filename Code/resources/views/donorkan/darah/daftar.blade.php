@extends('donorkan.main')

@section('content')

  <!-- Data Table area Start-->
  <div class="data-table-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <center><p><i>Riwayat Minta Darah</i> </p></center>
                    <p><i>* Apabila sudah mendapatkan pendonor dan ingin menutup postingan anda tekan tombol "Selesai" </i></p>
                   
                    <div class="card">
                        <div class="card-body">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Pasien</th>
                                        <th>Gol. Darah </th>
                                        <th>Usia</th>
                                        <th>Jenis Kelamin</th>
                                        <th>Riwayat Penyakit</th>
                                        <th>Tanggal</th>
                                        <th>Status</th>
                                        <th>Ubah Status</th>
                                        <th>Detail</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $no=1; ?>
                                        @foreach ($transaksi as $item)
                                        <tr>
                                        <td>{{ $no }}</td>
                                        <td>{{ $item->nama }}</td>
                                        <td>{{ $item->golongan_darah }}</td>
                                        <td>{{ $item->usia }}</td>
                                        <td>{{ $item->jenis_kelamin }}</td>
                                        <td>{{ $item->riwayat_penyakit }}</td>
                                        <td>{{ date("d F Y", strtotime($item->created_at)) }}</td>
                                            @if($item->status == 'Selesai')
                                                 <td >Selesai</td>
                                            @else
                                                 <td >Pending</td>
                                            @endif
                                        <td>
                                            <form method="POST" action="{{ route('name.donorkan.darahstatus',['id'=>$item->id]) }}" enctype="multipart/form-data">
                                                {{csrf_field()}}
                                                @if($item->status == 'Pending')
                                                    <button class="btn btn-primary" name="status" value="selesai" type="submit">Selesai</button>
                                                @endif
                                            </form>
                                        </td>
                                        <td><a href="{{ url('/Home/Riwayat/Minta/Darah/Detail',['id'=>Crypt::encrypt($item->id)]) }}" class="btn btn-medium btn-success"> Detail</a></td>
                                     
                                        </tr>
                                    <?php $no++; ?>    
                                        @endforeach
                                    
                                </tbody>
                                
                                </table>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    <!-- Data Table area End-->

@endsection