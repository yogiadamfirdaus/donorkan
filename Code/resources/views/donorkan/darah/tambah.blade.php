@extends('donorkan.main')


@section('content')
<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="http://cdn.leafletjs.com/leaflet-0.7.3/leaflet.css"/>
</head>
<body>
<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-header">
                        <strong>Minta Darah</strong> 
                    </div>
                    <div class="card-body card-block">
                        <form action="{{ route('name.donorkan.darah.tambah') }}" method="POST" enctype="multipart/form-data" class="form-horizontal">
                            {{csrf_field()}}

                            <div class="row form-group">
                                <div class="col col-md-3">
                                    <label for="text-input" class=" form-control-label">Golongan Darah</label>
                                </div>
                                <div class="col-12 col-md-9">
                                    <input type="text"  name="golongan_darah" placeholder="Golongan Darah" class="form-control" required>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-3">
                                    <label for="text-input" class=" form-control-label">Nama Pasien</label>
                                </div>
                                <div class="col-12 col-md-9">
                                    <input type="text"  name="nama" placeholder="Nama Pasien" class="form-control" required>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-3">
                                    <label for="text-input" class=" form-control-label">Usia</label>
                                </div>
                                <div class="col-12 col-md-9">
                                    <input type="text"  name="usia" placeholder="Usia" class="form-control" required>
                                </div> 
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-3">
                                    <label for="text-input" class=" form-control-label">Jenis Kelamin</label>
                                </div>
                                <div class="col-12 col-md-9">
                                    <select class="form-control" name="jenis_kelamin">
                                        <option value="laki-laki">laki-laki</option> 
                                        <option value="perempuan">perempuan</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-3">
                                    <label for="text-input" class=" form-control-label">Riwayat Penyakit</label>
                                </div>
                                <div class="col-12 col-md-9">
                                    <input type="text"  name="riwayat_penyakit" placeholder="Riwayat Penyakit" class="form-control" required>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-3">
                                    <label for="text-input" class=" form-control-label">Alamat</label>
                                </div>
                                <div class="col-12 col-md-9">
                                    <textarea type="text"  name="alamat" placeholder="Alamat" class="form-control" required> </textarea>
                                    <p><i>Contoh : jalan veteran no 5 rt 04 rw 03, gresik, jawa timur</i></p>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-12 col-md-9">
                                <input id="latInput" type="hidden" name="latitude"class="form-control" required>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-12 col-md-9">
                                <input id="lngInput" type="hidden" name="longitude"class="form-control" required>
                                </div>
                            </div>
                            <div id="map"></div>
                            

    <script src="http://cdn.leafletjs.com/leaflet-0.7.3/leaflet.js"></script>
    <script src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
    <script>
    	function addMapPicker() {
	    	var mapCenter = [22, 87];
            var map = L.map('map').setView([-7.158018477193084,  112.65608310699461], 12);
			 L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
				maxZoom: 18,
				attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
			}).addTo(map);
		var marker = L.marker(mapCenter).addTo(map);
		var updateMarker = function(lat, lng) {
		    marker
		        .setLatLng([lat, lng])
		        .bindPopup("Lokasi Pasien")
		        .openPopup();
		    return false;
		};
		map.on('click', function(e) {
		    $('#latInput').val(e.latlng.lat);
		    $('#lngInput').val(e.latlng.lng);
		    updateMarker(e.latlng.lat, e.latlng.lng);
	    	});
	    	var updateMarkerByInputs = function() {
			return updateMarker( $('#latInput').val() , $('#lngInput').val());
		}
		$('#latInput').on('input', updateMarkerByInputs);
		$('#lngInput').on('input', updateMarkerByInputs);
    	}
    	
	$(document).ready(function() {
	    addMapPicker();
	});
    </script>
    <style>
		input {
		    margin-bottom : 2px;
		}
		#map {
		    
		    height : 300px;
		}
    </style>
    </script>                                                        
                            <div class="card-footer">
                                <button class="btn btn-info" type="submit">Submit</button>
                            </div>

                        </form>
                    </div>                         
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>

@endsection

