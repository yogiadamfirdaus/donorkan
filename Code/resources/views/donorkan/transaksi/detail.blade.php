
@extends('donorkan.main')

@section('content')
<!DOCTYPE html>
<html lang="en">
<head>
<link rel="stylesheet" href="http://cdn.leafletjs.com/leaflet-0.7.3/leaflet.css"/>
</head>

<body>
    
<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-lg-6">
            <center><p><i>Riwayat Donor Darah - Detail</i> </p></center>
                <div class="card">
                    <div class="card-header">
                        <strong>Lokasi Pasien</strong> 
                    </div>
                    <div class="card-body card-block">
                            <div id="map"></div>
                    </div>                         
                </div>
            </div>

    <div class="col-lg-4 col-md-6">
        <aside class="profile-nav alt">
            <section class="card">

                <div class="card-header user-header alt bg-dark">
                    <div class="media">
                        <div class="media-body">
                            <h4 class="text-light display-6">Informasi Pasien</h4>
                        </div>
                    </div>
                </div>
                <ul class="list-group list-group-flush">
                    <li class="list-group-item">
                         <i>Nama : </i> {{$dt->nama}}
                    </li>
                    <li class="list-group-item">
                         <i>Golongan Darah :  </i> {{$dt->golongan_darah}}
                    </li>
                    <li class="list-group-item">
                         <i>Email :  </i> {{ $dt->user->email}}
                    </li>
                    <li class="list-group-item">
                         <i>Usia :     </i> {{$dt->usia}}
                    </li>
                    <li class="list-group-item">
                         <i>Jenis Kelamin :       </i> {{$dt->jenis_kelamin}}
                    </li>
                    <li class="list-group-item">
                         <i>Riwayat Penyakit :      </i> {{$dt->riwayat_penyakit}}
                    </li>
                    <li class="list-group-item">
                         <i>Tanggal :     </i> {{ date("d F Y", strtotime($dt->created_at)) }}
                    </li>

                    <li class="list-group-item">
                         <i>Alamat Pasien :     </i>   {{$dt->lokasi->alamat}}
                    </li>

                    

                </ul>

            </section>
        </aside>
    </div>
            
        </div>
    </div>
</div>
    <script src="http://cdn.leafletjs.com/leaflet-0.7.3/leaflet.js"></script>
    <script src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
    <script>
    	function addMapPicker() {
                        var mapCenter = [{{ $dt->lokasi->latitude }}, {{ $dt->lokasi->longitude }}];
                        var map = L.map('map').setView([{{ $dt->lokasi->latitude }}, {{ $dt->lokasi->longitude }}], 12);
                        L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
                            maxZoom: 18,
                            attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
                        }).addTo(map);
                    
                    L.marker(mapCenter).addTo(map);
                  //  .bindPopup("Lokasi");
    	}
    	
	$(document).ready(function() {
	    addMapPicker();
	});
    </script>
            <style>
                input {
                    margin-bottom : 2px;
                }
                #map {
                    
                    height : 300px;
                }
            </style>
    </script>

</body>

</html>


@endsection