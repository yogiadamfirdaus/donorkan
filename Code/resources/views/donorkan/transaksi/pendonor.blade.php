@extends('donorkan.main')

@section('content')

  <!-- Data Table area Start-->
  <div class="data-table-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <center><p><i>Lihat Pendonor</i> </p></center>
                    <p><i>* Apabila pendonor yang telah melakukan donor darah tekan tombol "Sudah Donor"</i></p>

                    <div class="card">
                        <div class="card-body">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Pasien</th>
                                        <th>Gol. Darah </th>
                                        <th>Pendonor</th>
                                        <th>Email Pendonor</th>
                                        <th>tanggal</th>
                                        <th>Status</th>
                                        <th>Ubah Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $no=1; ?>
                                        @foreach ($pendonor as $item)
                                        <tr>
                                        <td>{{ $no }}</td>
                                        <td>{{ $item->darah->nama }}</td>
                                        <td>{{ $item->darah->golongan_darah }}</td>
                                        <td>{{ $item->user->name }}</td>
                                        <td>{{ $item->user->email}}</td>
                                        <td>{{ date("d F Y", strtotime($item->created_at)) }}</td>
                                            @if($item->konfirmasi == 'SudahDonor')
                                                 <td >Sudah Donor</td>
                                            @else
                                                 <td >Belum Donor</td>
                                            @endif
                                        <td>
                                            <form method="POST" action="{{ route('name.donorkan.transaksi.pendonorstatus',['id'=>$item->id]) }}" enctype="multipart/form-data">
                                                {{csrf_field()}}
                                                @if($item->konfirmasi == 'BelumDonor')
                                                    <button class="btn btn-success" name="konfirmasi" value="sudahdonor" type="submit">Sudah Donor</button>
                                                @else
                                                  
                                                @endif
                                            </form>
                                        </td>

                                        </tr>
                                    <?php $no++; ?>    
                                        @endforeach
                                    
                                </tbody>
                                
                                </table>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    <!-- Data Table area End-->

@endsection