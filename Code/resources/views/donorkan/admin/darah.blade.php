@extends('donorkan.main')

@section('content')

 <!-- Data Table area Start-->
 <div class="data-table-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="data-table-list">
                    <center><p><i>Managemen Darah</i> </p></center>
                   
                    <div class="card">
                        <div class="card-body">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Pasien</th>
                                        <th>Gol. Darah </th>
                                        <th>Usia</th>
                                        <th>Jenis Kelamin</th>
                                        <th>Riwayat Penyakit</th>
                                        <th>Tanggal</th>
                                        <th>Status</th>
                                        <th>Detail</th>
                                        <th>Hapus</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $no=1; ?>
                                        @foreach ($transaksi as $item)
                                        <tr>
                                        <td>{{ $no }}</td>
                                        <td>{{ $item->nama }}</td>
                                        <td>{{ $item->golongan_darah }}</td>
                                        <td>{{ $item->usia }}</td>
                                        <td>{{ $item->jenis_kelamin }}</td>
                                        <td>{{ $item->riwayat_penyakit }}</td>
                                        <td>{{ date("d F Y", strtotime($item->created_at)) }}</td>
                                            @if($item->status == 'Selesai')
                                                 <td >Selesai</td>
                                            @else
                                                 <td >Pending</td>
                                            @endif
                                        <td><a href="{{ url('/Home/Admin/Managemen/Darah/Detail',['id'=>Crypt::encrypt($item->id)]) }}" class="btn btn-medium btn-success"> Detail</a></td>
                                        <td><a href="{{ url('/Home/Admin/Managemen/Darah/Hapus',['id'=>Crypt::encrypt($item->id)]) }}" class="btn btn-danger">Hapus</a></td>
                                        
                                        </tr>
                                    <?php $no++; ?>    
                                        @endforeach
                                    
                                </tbody>
                                
                                </table>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    <!-- Data Table area End-->

@endsection