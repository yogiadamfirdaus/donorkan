@extends('donorkan.main')

@section('content')

<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-md-6 col-lg-3">
            <a href="{{ route('name.donorkan.admin.jumlah.jumlahpendonor') }}">
                <div class="card bg-flat-color-1 text-light">
                    <div class="card-body">
                        <div class="h4 m-0">{{ $task2->count() }}</div>
                        <div>Jumlah pendonor</div>
                    </div>
                </div>
            </a>
            </div>
            <div class="col-md-6 col-lg-3">
            <a href="{{ route('name.donorkan.admin.jumlah. jumlahdonordarah') }}">
                <div class="card bg-flat-color-4 text-light">
                    <div class="card-body">
                        <div class="h4 m-0">{{ $task3->count() }}</div>
                        <div>Jumlah sudah donor </div>
                    </div>
                </div>
            </a>
            </div>
            <div class="col-md-6 col-lg-3">
            <a href="{{ route('name.donorkan.admin.jumlah.jumlahmintadarah') }}">
                <div class="card bg-flat-color-3 text-light">
                    <div class="card-body">
                        <div class="h4 m-0">{{ $task4->count() }}</div>
                        <div>Jumlah minta darah</div>
                    </div>
                </div>
            </a>
            </div>
            <div class="col-md-6 col-lg-3">
            <a href="{{ route('name.donorkan.admin.jumlah.jumlahbelumdonor') }}">
                <div class="card bg-flat-color-5 text-light">
                    <div class="card-body">
                        <div class="h4 m-0">{{ $task6->count() }}</div>
                        <div>Jumlah belum donor</div>
                    </div>
                </div>
            </a>
            </div>
        </div>
    </div>
</div>



<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">
         
            <div class="col-lg-6 col-md-6">
                <div class="card">
                    <div class="card-header ">
                    <h6>Pasien</h6>
                    </div>
                    <div class="card-body">
                        
                            @foreach ($task5 as $item )
                            <div class="col-md-6 ">
                                    <h5 class="text-sm-center mt-2 mb-1">- {{ $item->darah->nama }}</h5>
                            </div>
                            @endforeach
                        </div>
                        <div class="mx-auto d-block">
                            <div class="corner-ribon black-ribon">
                        </div>
                        <hr>
                        <div >
                            <center>
                            <a href="{{ route('name.donorkan.admin.pendonor') }}" class="btn social tumblr">Lihat Pendonor</a>
                            </center>
                        </div>
                        <hr>
                    </div>
                </div>
            </div>

         
        </div>
    </div>
</div>




@endsection
