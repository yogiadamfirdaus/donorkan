@extends('donorkan.main')

@section('content')

  <!-- Data Table area Start-->
  <div class="data-table-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                    <div class="card bg-flat-color-1 text-light">
                        <div class="card-body">
                            <div class="h4 m-0">{{ $task1->count() }}</div>
                            <div>Jumlah pendonor</div>
                        </div>
                    </div>
                    
                    <center><p><i>Jumlah Pendonor</i> </p></center>
                    

                    <div class="card">
                        <div class="card-body">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Pasien</th>
                                        <th>Gol. Darah </th>
                                        <th>Pendonor</th>
                                        <th>Email Pendonor</th>
                                        <th>tanggal</th>
                                        <th>Status</th>
                                        <th>Detail</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $no=1; ?>
                                        @foreach ($task1 as $item)
                                        <tr>
                                        <td>{{ $no }}</td>
                                        <td>{{ $item->darah->nama }}</td>
                                        <td>{{ $item->darah->golongan_darah }}</td>
                                        <td>{{ $item->user->name }}</td>
                                        <td>{{ $item->user->email}}</td>
                                        <td>{{ date("d F Y", strtotime($item->created_at)) }}</td>
                                            @if($item->konfirmasi == 'SudahDonor')
                                                 <td >Sudah Donor</td>
                                            @else
                                                 <td >Belum Donor</td>
                                            @endif
                                            <td><a href="{{ url('/Home/Admin/Pendonor/Detail',['id'=>Crypt::encrypt($item->id)]) }}" class="btn btn-medium btn-success"> Detail</a></td>

                                        </tr>
                                    <?php $no++; ?>    
                                        @endforeach
                                    
                                </tbody>
                                
                                </table>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    <!-- Data Table area End-->

@endsection