<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <meta charset="utf-8">
  <title>donorkan</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <meta name="description" content="Your page description here" />
  <meta name="author" content="" />

  <!-- css      {{ asset('Remember/     ') }}         --> 
  <link href="{{ asset('Remember/css/bootstrap.css') }}" rel="stylesheet" />
  <link href="{{ asset('Remember/css/bootstrap-responsive.css') }}" rel="stylesheet" />
  <link href="{{ asset('Remember/css/prettyPhoto.css') }}" rel="stylesheet" />
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
  <link href="{{ asset('Remember/css/style.css') }}" rel="stylesheet">

  <!-- Theme skin -->
  <link id="t-colors" href="color/default.css" rel="stylesheet" />

  <!-- Fav and touch icons -->
  <link rel="apple-touch-icon-precomposed" sizes="144x144" href=" {{ asset('Remember/ico/apple-touch-icon-144-precomposed.png') }}" />
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href=" {{ asset('Remember/ico/apple-touch-icon-114-precomposed.png') }}" />
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href=" {{ asset('Remember/ico/apple-touch-icon-72-precomposed.png') }}" />
  <link rel="apple-touch-icon-precomposed" href=" {{ asset('Remember/ico/apple-touch-icon-57-precomposed.png') }}" />
  <link rel="shortcut icon" href=" {{ asset('Remember/ico/favicon.png') }}" />

  <!-- =======================================================
    Theme Name: Remember
    Theme URL: https://bootstrapmade.com/remember-free-multipurpose-bootstrap-template/
    Author: BootstrapMade.com
    Author URL: https://bootstrapmade.com
  ======================================================= -->
</head>

<body>
  <div id="wrapper">
    <!-- start header -->
    <header>
      <div class="top">
        <div class="container">
          <div class="row">
           
          </div>
        </div>
      </div>
      <div class="container">


        <div class="row nomargin">
          <div class="span4">
            <div class="logo">
              <h1><a href="index.html"><i class="icon-tint"></i> donorkan</a></h1>
            </div>
          </div>
          <div class="span8">
            <div class="navbar navbar-static-top">
              <div class="navigation">
                <nav>
                  <ul class="nav topnav">
                @if (Route::has('login'))
                        @auth
                            @if(Auth::user()->akses == 'admin')
                            <li class="nav topnav">
                                <a href="{{route('home.admin')}}">Home</a>
                            </li>
                            @endif
                            @if(Auth::user()->akses == 'customer')
                            <li class="nav topnav">
                                <a href="{{route('home')}}">Home</a>
                            </li>
                            @endif
                        @else
                        <li class="nav topnav">
                            <a href="{{ route('login') }}">Login</a>
                        </li>
                        @if (Route::has('register'))
                        <li class="nav topnav">
                            <a href="{{ route('register') }}">Register</a>
                        </li>
                        @endif
                        @endauth
                @endif
                      
                   
                  </ul>
                </nav>
              </div>
              <!-- end navigation -->
            </div>
          </div>
        </div>
      </div>
    </header>
    <!-- end header -->

    <!-- section intro -->
    <section id="intro">
      <div class="intro-content">
      <blockquote>
      Sekecil apapun kebaikan yang kita lakukan tak kan pernah sia-sia.
       </blockquote>
      
        <div>
          <a href="{{ route('darah') }}" class="btn btn-danger">Donor
          </a>
        </div>
      </div>
    </section>
    <!-- /section intro -->

    <section id="content">
      <div class="container">


        <div class="row">
          <div class="span12">
            <div class="row">
              

              <div class="span4">
                <div class="box aligncenter">
                  <div class="icon">
                    <span class="badge badge-info badge-circled">1</span>
                    <i class="ico icon-file-text icon-5x"></i>
                  </div>
                  <div class="text">
                    <h4><strong>Minta darah</strong></h4>
                    <p>
                      Menulis informasi.
                    </p>
                  </div>
                </div>
              </div>
              <div class="span4">
                <div class="box aligncenter">
                  <div class="icon">
                    <span class="badge badge-important badge-circled">2</span>
                    <i class="ico icon-user icon-5x"></i>
                  </div>
                  <div class="text">
                    <h4><strong>Donor darah</strong></h4>
                    <p>
                      Menemukan darah.
                    </p>
                  </div>
                </div>
              </div>
              <div class="span4">
                <div class="box aligncenter">
                  <div class="icon">
                    <span class="badge badge-success badge-circled">3</span>
                    <i class="ico icon-thumbs-up icon-5x"></i>
                  </div>
                  <div class="text">
                    <h4><strong>Selesai</strong></h4>
                    <p>
                      Misi selesai.
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>


       

      </div>
    </section>

    <section id="offer">
      <div class="container">
        <div class="row">
          <div class="span12">
            <div class="row">
              <div class="span12">
                <div class="aligncenter">
                  <h3>Cara kerja</h3>
                  <blockquote>
                    Butiran embun pagi selalu mengajarkan bahwa akan ada harapan disetiap lika-liku kehidupan.
                  </blockquote>
                </div>
              </div>
            </div>

            <div class="row">

              <div class="span3">
                <h5>Minta darah</h5>
                <p>Berikan beberapa informasi mengenai pasien yang sedang membutuhkan darah.</p>
              </div>
              <div class="span3">
                <h5>Donor darah</h5>
               <p>Pendonor mencari golongan darah yang sesuai dengan dirinya.</p>
              </div>
              <div class="span3">
                <h5>Aksi nyata</h5>
                <p>Pendonor melakukan donor darah sesuai dengan alamat pasien.</p>
              </div>
              
              <div class="span3">
                <h5>Selesai</h5>
                <p>Setelah misi selesai berikan apresiasi kepada pendonor dengan menekan tombol "Sudah Donor".</p>
              </div>
              
              
              

              
          </div>
        </div>

      </div>
    </section>

    <footer>
      <div id="sub-footer">
        <div class="container">
          <div class="row">
            <div class="span6">
              <div class="copyright">
                <p><span>&copy; 2020 donorkan</span></p>
              </div>

            </div>

            
          </div>
        </div>
      </div>
    </footer>
  </div>
  <a href="#" class="scrollup"><i class="icon-angle-up icon-rounded icon-bglight icon-2x"></i></a>

  <!-- javascript
    ==================================================        {{ asset('Remember/     ') }}         -->
  <!-- Placed at the end of the document so the pages load faster -->
  <script src="{{ asset('Remember/js/jquery.js') }} "></script>
  <script src="{{ asset('Remember/js/jquery.easing.1.3.js') }} "></script>
  <script src="{{ asset('Remember/js/bootstrap.js') }} "></script>
  <script src="{{ asset('Remember/js/modernizr.custom.js') }} "></script>
  <script src="{{ asset('Remember/js/toucheffects.js') }} "></script>
  <script src="{{ asset('Remember/js/google-code-prettify/prettify.js') }} "></script>
  <script src="{{ asset('Remember/js/jquery.prettyPhoto.js') }} "></script>
  <script src="{{ asset('Remember/js/portfolio/jquery.quicksand.js') }} "></script>
  <script src="{{ asset('Remember/js/portfolio/setting.js') }} "></script>
  <script src="{{ asset('Remember/js/animate.js') }} "></script>

  <!-- Template Custom JavaScript File -->
  <script src="{{ asset('Remember/js/custom.js') }} "></script>

</body>
       
</html>
